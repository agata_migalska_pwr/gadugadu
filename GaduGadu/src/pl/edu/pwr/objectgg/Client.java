package pl.edu.pwr.objectgg;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
	
	private static final int PORT = 5000;
	private static final String HOST = "127.0.0.1";

	public Object readObjectFromSocket() {
		Socket socket = null;
		InputStream inputStream = null;
		ObjectInputStream objectInputStream = null;
		Object object = null;
		try {
			socket = new Socket(HOST, PORT);
			inputStream = socket.getInputStream();
			objectInputStream = new ObjectInputStream(inputStream);
			object = objectInputStream.readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			closeResource(objectInputStream);
			closeResource(inputStream);
			closeResource(socket);
		}
		return object;
	}
	
	public String readStringFromSocket() {
		Socket socket = null;
		InputStream inputStream = null;
		InputStreamReader inputStreamReader = null;
		BufferedReader bufferedReader = null;
		String readString = null;
		try {
			socket = new Socket(HOST, PORT);
			inputStream = socket.getInputStream();
			inputStreamReader = new InputStreamReader(inputStream);
			bufferedReader = new BufferedReader(inputStreamReader);
			readString = bufferedReader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			closeResource(bufferedReader);
			closeResource(inputStreamReader);
			closeResource(inputStream);
			closeResource(socket);
		}
		return readString;
	}
	
	public void writeObjectToSocket(Object objectToWrite) {
		Socket socket = null;
		OutputStream outputStream = null;
		ObjectOutputStream objectOutputStream = null;
		try {
			socket = new Socket(HOST, PORT);
			outputStream = socket.getOutputStream();
			objectOutputStream = new ObjectOutputStream(outputStream);
			objectOutputStream.writeObject(objectToWrite);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			closeResource(objectOutputStream);
			closeResource(outputStream);
			closeResource(socket);
		}
	}
	
	public void writeStringToSocket(String stringToWrite) {
		Socket socket = null;
		OutputStream outputStream = null;
		PrintWriter printWriter = null;
		try {
			socket = new Socket(HOST, PORT);
			outputStream = socket.getOutputStream();
			printWriter = new PrintWriter(outputStream);
			printWriter.println(stringToWrite);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			closeResource(printWriter);
			closeResource(outputStream);
			closeResource(socket);
		}
	}
	
	public static void main(String[] args) {
		
	}

	private static void closeResource(Closeable closeableResource) {
		if (closeableResource != null) {
			try {
				closeableResource.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
